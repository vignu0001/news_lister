from django.shortcuts import render
import requests
from django.http import Http404, HttpResponse
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

from django.views.generic import TemplateView

class News(TemplateView):
	def get(self,request):
		search_result = {}
		if 'news' in request.GET :
			news = request.GET['news']
			url = 'http://content.guardianapis.com/search?api-key=test&q=%s' % news+'&show-%20fields=thumbnail,headline&page=1&page-size=10'
			response = requests.get(url)
			search_was_successful = (response.status_code == 200)  # 200 = SUCCESS
			search_result = response.json()
			list = []
			for i in range(1,search_result['response']['pageSize']):
				list.append(i)
		# return HttpResponse(search_result['response']['results'][0]['id'])
		return render(request,  'news/index.html',locals())
